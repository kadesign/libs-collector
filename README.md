# Libraries collector

Place to collect interesting and useful libraries/apps/etc

## Cloud services

- [Supabase](https://supabase.io) - PostgreSQL DBaaS

## Utilities

- [ngrok](https://ngrok.com) - tunnels to localhost
- [surge](https://surge.sh) - deployment of static webapps from terminal
- [license](https://github.com/Ovyerus/license) - license generator

## JS

- [tmi.js](https://github.com/tmijs/tmi.js) - interaction with Twitch chat
- [Twurple / Twitch.js](https://github.com/twurple/twurple) - Twitch API wrapper
- [QR Scanner by Nimiq](https://github.com/nimiq/qr-scanner)
- [joi](https://joi.dev/) - data validation
- [Day.js](https://day.js.org/en/) - date/time manipulation
- [Showdown](https://github.com/showdownjs/showdown) - Markdown to HTML converter (has implementations for several major frontend frameworks)
  - [Vue.js version](https://vue-showdown.js.org/)
- [PeerJS](https://peerjs.com) - WebRTC peer-to-peer library
- [ml5.js](https://ml5js.org) - machine learning for Web
- [pkg](https://www.npmjs.com/package/pkg) - create an executable package of Node project
- [parcel.js](https://v2.parceljs.org/) - code compiler (alternative to Webpack)
- [express-ws-routes](https://github.com/amekkawi/express-ws-routes) - Express router add-on enabling WebSocket routes
- [DOMPurify](https://github.com/cure53/DOMPurify) - XSS sanitizer
- [Prisma](https://www.prisma.io/) - database management library
- [Feathers](https://feathersjs.com/) - lightweight REST API framework
- [Stencil](https://stenciljs.com/) - toolchain for building Web Components
- [grammY](https://grammy.dev/) - Telegram bot framework
- [Tabler](https://tabler.io/) - UI kit for dashboards
- [SinonJS](https://sinonjs.org/) - mocks for testing

## Others

- [Nginx Proxy Manager](https://github.com/jc21/nginx-proxy-manager) - UI for Nginx
